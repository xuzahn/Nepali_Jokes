package com.xuzah.nepaliquotes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by 5442 on 2/17/2018.
 */

public class Pager_Adapter extends FragmentPagerAdapter {


    public Pager_Adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new Quotes();
        } else {
            return new Jokes();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Quotes";
            case 1:
               return "Jokes";
            default:
                return null;
        }
    }
}
